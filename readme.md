# Application d'Extraction de Texte

L'application d'extraction de texte est un outil Web conçu pour extraire du texte à partir de divers types de documents tels que PDF, Word (docx), Excel (xlsx) et les fichiers vidéos (mp4, mov, avi). Il utilise la bibliothèque Streamlit pour créer une interface utilisateur simple et intuitive.

La magie réside dans le fait qu'il utilise les bibliothèques python `docx`, `fitz`, `PyMuPDF`, `pandas`, `moviepy` et `speech_recognition` pour lire, extraire et transcrire du texte à partir de ces types de fichiers. Que ce soit des paragraphes dans un document Word, du texte dans un fichier PDF, des cellules dans un fichier Excel ou de l'audio transcrit dans une vidéo, l'application est capable de le gérer.

## Prérequis

- Python 3.10 ou supérieur.

## Installation

Suivez les étapes ci-dessous pour installer et exécuter l'application :

1. Clonez le dépôt :
    ```
    git clone https://gitlab.com/ballo.issiaka/extracteur-de-texte.git
    ```

2. Naviguez jusqu'au répertoire du projet :
    ```
    cd <nom_du_projet>
    ```

3. (Optionnel) Il est recommandé de créer un environnement virtuel pour isoler les dépendances de ce projet :
    ```
    python3 -m venv env
    source env/bin/activate  # Pour les utilisateurs Unix/Linux
    .\env\Scripts\activate  # Pour les utilisateurs Windows
    ```

4. Installez les dépendances nécessaires à partir du fichier `requirements.txt` :
    ```
    pip install -r requirements.txt
    ```

## Utilisation

Pour utiliser l'application, suivez les instructions ci-dessous :

1. Exécutez l'application Streamlit à partir de la ligne de commande :
    ```
    streamlit run main.py
    ```

2. Ouvrez votre navigateur Web et accédez à `localhost:8501` (ou l'URL fournie dans la sortie de la commande précédente).

3. Vous serez accueilli par une interface utilisateur simple. Utilisez l'option pour télécharger le fichier à partir de votre système local.

4. Choisissez le format de sortie que vous souhaitez (PDF ou TXT) et cliquez sur le bouton pour extraire le texte.

5. Le texte extrait sera affiché et vous aurez également l'option de le télécharger.

## Contribution

Les contributions sont les bienvenues. Pour toute modification majeure, veuillez ouvrir une issue pour en discuter avant de faire une pull request.

## Licence

Cette application est distribuée sous la licence libre. Voir `LICENCE` pour plus d'informations.

## Contact

Si vous avez des questions ou des problèmes concernant l'application, n'hésitez pas à me contacter :

- Issiaka Ballo
- Email : isballo@gs2e.ci

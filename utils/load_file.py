
import pandas as pd
from docx import Document
import os
import fitz  # PyMuPDF
from moviepy.editor import VideoFileClip
from pydub import AudioSegment
import speech_recognition as sr
import streamlit as st

def read_docx(file):
    doc = Document(file)
    text = ' '.join([paragraph.text for paragraph in doc.paragraphs])
    return text

def read_pdf(file):
    doc = fitz.open(stream=file, filetype='pdf')
    text = ' '.join([page.get_text() for page in doc])
    return text

def read_excel(file):
    df = pd.read_excel(file)
    return df.to_string(index=False)

def transcribe_audio_from_video(file, extension):
    temp_video_file = "temp_video" + extension
    with open(temp_video_file, "wb") as out_file:
        out_file.write(file.getbuffer())
    st.write('Chargement de la vidéo...')
    video = VideoFileClip(temp_video_file)
    audio = video.audio
    audio_file = "temp_audio.wav"
    audio.write_audiofile(audio_file)
    sound = AudioSegment.from_file(audio_file, format="wav")
    sound.export("temp_audio.wav", format="wav")
    r = sr.Recognizer()
    with sr.AudioFile("temp_audio.wav") as source:
        audio = r.record(source)
    text = r.recognize_google(audio, language="fr-FR", show_all=True)
    video.close()
    os.remove(temp_video_file)
    os.remove(audio_file)
    return str(text)

def load_document(file):
    _, extension = os.path.splitext(file.name)
    if extension == '.docx':
        return read_docx(file)
    elif extension == '.pdf':
        return read_pdf(file)
    elif extension == '.xlsx' or extension == '.xls':
        return read_excel(file)
    elif extension in ['.mp4', '.mov', '.avi']:
        return transcribe_audio_from_video(file, extension)
    else:
        st.write("Unsupported file format")
        return None

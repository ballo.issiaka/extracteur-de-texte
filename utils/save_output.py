import json
def save_output(format, text, filename):
    # text = text.encode('unicode_escape').decode('utf-8') 
    if format == 'txt':
        with open(f'{filename}.txt', 'w', encoding='utf-8') as f:
            f.write(text)
        return f'{filename}.txt'
    elif format == 'json':
        with open(f'{filename}.json', 'w', encoding='utf-8') as f:
            json.dump(text, f)
        return f'{filename}.json'
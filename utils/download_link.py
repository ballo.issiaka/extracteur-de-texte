
import base64
def get_download_link(output_file, output_format,output_filename):
    with open(output_file, 'rb') as f:
        file_data = f.read()
    b64 = base64.b64encode(file_data).decode()
    return f'<a href="data:application/octet-stream;base64,{b64}" download="{output_filename}.{output_format}">Télécharger le fichier de sortie</a>'

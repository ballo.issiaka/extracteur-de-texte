import streamlit as st
from utils.load_file import load_document
from utils.save_output import save_output
from utils.download_link import get_download_link

st.title('Analyseur de documents')

uploaded_file = st.sidebar.file_uploader("Choisissez un fichier DOCX, PDF, Excel ou Video", type=['docx', 'pdf', 'xlsx', 'xls', 'mp4', 'mov', 'avi'])
output_format = st.sidebar.selectbox('Sélectionnez le format de sortie', ('txt', 'json'))
output_filename = st.sidebar.text_input('Entrez le nom du fichier de sortie') or "default"

extract_button = st.sidebar.button('Extraire le texte')  # Ajout du bouton

if extract_button:  # Condition qui vérifie si le bouton a été cliqué
    if uploaded_file is not None and output_format is not None and output_filename:
        document_text = load_document(uploaded_file)
        st.write('### Contenu du document :')
        st.write(document_text)
        output_file = save_output(output_format, document_text, output_filename)
        st.write('### Fichier de sortie :')
        st.write(output_file)

        download_link = get_download_link(output_file, output_format, output_filename)
        st.markdown(download_link, unsafe_allow_html=True)
    else:
        st.error("Veuillez fournir tous les champs requis avant d'extraire.")
